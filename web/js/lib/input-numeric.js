angular.module("input-numeric", []);
angular.module("input-numeric").directive('inputNumeric', ['$timeout', 'inputNumericService', function($timeout, inputNumericService) {
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, ctrl) {
         var fractionSize = parseInt(attrs.inputNumeric);
         if (isNaN(fractionSize)) fractionSize = 0;
         var execute = function () {
             scope.$apply(function () {
                 ctrl.$setViewValue(inputNumericService.format(ctrl.$modelValue, fractionSize));
                 ctrl.$render();
             });
         };
         element.bind('keyup', function () {
             execute();
         });
         
         ctrl.$parsers.push(function(value) {
             return (inputNumericService.toNumber(value) / (Math.pow(10, fractionSize))).toFixed(fractionSize);
         });         
         
         $timeout(function () { 
             execute();
         }, 500);
      }
   };
}]);
angular.module("input-numeric").factory("inputNumericService", [ "$filter", function ($filter) {
    var _otherCharacters = /[^0-9]/g;
    var _zeros = /^0+/;
    
    var _format = function (string, fractionSize) {
        return $filter("number")((_toNumber(string) / Math.pow(10, fractionSize)).toFixed(fractionSize), fractionSize);
    };
    
    var _toNumber = function (string) {
        if (!string) return 0;
        if (angular.isNumber(string)) string = string.toString();
        return string.replace(_otherCharacters, '').replace(_zeros, '') || 0;
    };
    
    return {
        format: _format,
        toNumber: _toNumber
    };
}]);