var app = angular.module('AngularJSApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'ui.date', 'input-numeric']);

app.config(['$compileProvider', function ($compileProvider) {
    $compileProvider.debugInfoEnabled(false);
}]);

app.config(['$httpProvider',
    function ($httpProvider) {
        $httpProvider.defaults.headers.common["Content-Type"] = "application/json; charset=utf-8";
}]);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/employees', {
            templateUrl: 'views/employees.html',
            controller: 'EmployeesController'
        })     
        .when('/login', {
            templateUrl: 'views/login.html',
            controller: 'LoginController'
        })      
        .otherwise({
            redirectTo: '/login'
        });
});

app.constant("AppConstants", {
    DATE_OPTIONS: {
        changeYear: true,
        changeMonth: true,
        dateFormat: "dd/mm/yy",
        minDate: new Date(1910, 0, 1),
        yearRange: "1910:+10"
    },
    MESSAGES: {
        CONFIRM_DELETE: "Confirma exclusão?",
        LOGIN_NOT_FOUND: 'Login ou senha não encontrados',    
        REQUIRED: 'Campo Obrigatório',
        RESPONSE_ERROR: "Ocorreu um erro de comunicação com o servidor, tente novamente mais tarde ou entre em contato com o suporte."
    },
    MODAL_MESSAGE: {
        DANGER: 'danger',
        INFO: 'info',
        QUESTION: 'question'
    }
});

app.service("AppService", function($rootScope, AppConstants, $uibModal, $location, $route) {
    var _self = this;
    
    _self.loadRoute = function(route, reload) {
        if ($location.path() === route) {
            if (reload) $route.reload();
        } else {
            $location.path(route);
        }
    };
    
    _self.logoff = function() {
        _self.setLoggedUser(null);
        _self.loadRoute("/login", true);
    };
    
    _self.openModalMessage = function(mensagem, tipo, yesFunction, closeFunction) {
        if (!tipo) {
            tipo = AppConstants.MODAL_MESSAGE.INFO;
        }
        
        return $uibModal.open({ 
            backdrop: 'static', 
            controller: function($scope, $uibModalInstance, mensagem, tipo, AppConstants) {
                $scope.MODAL_MESSAGE = {DANGER: AppConstants.MODAL_MESSAGE.DANGER,
                                        INFO: AppConstants.MODAL_MESSAGE.INFO,
                                        QUESTION: AppConstants.MODAL_MESSAGE.QUESTION};
                $scope.mensagem = mensagem;
                $scope.tipo = tipo;
                
                if (closeFunction) {
                    $scope.close = closeFunction;
                } else {
                    $scope.close = function() {
                        $uibModalInstance.close();
                    };
                }
                
                if (tipo === AppConstants.MODAL_MESSAGE.QUESTION) {
                    $scope.no = function() {
                        $scope.close();
                    };

                    $scope.yes = yesFunction;
                }
            },
            keyboard: (tipo !== AppConstants.MODAL_MESSAGE.QUESTION),
            resolve: {
                mensagem: function() { return mensagem; },
                tipo: function() { return tipo; }
            },
            templateUrl: 'modalMessage.html' 
        });        
    };
    
    _self.setLoggedUser = function(data) {
        var loggedUser = null;
        if (data) {
            loggedUser = {};
            angular.copy(data, loggedUser);
        }
        $rootScope.loggedUser = loggedUser;
    };
});

app.run(function(AppConstants, AppService) {
});

app.controller("AppController", function($scope, AppService) {
    $scope.logoff = function() {
        AppService.logoff();
    };
});

app.controller("EmployeesController", function($scope, $rootScope, AppConstants, AppService, $http, $filter) {
    if (!$rootScope.loggedUser) {
        AppService.loadRoute("login");
        return;
    }
    
    $scope.item = {};
    var itens = [];  
    $scope.itensFiltered = [];
    $scope.selectedItem = null;
    $scope.viewType = "list";
    
    $scope.DATE_OPTIONS = AppConstants.DATE_OPTIONS;
    $scope.MESSAGES = {REQUIRED: AppConstants.MESSAGES.REQUIRED};
    
    var setFocus = function() {
        setTimeout(function() {
            cursorToEnd($("#nome"));
        }, 500);        
    };
    
    $scope.add = function() {
        $scope.formCrud.$submitted = false;
        $scope.item = {};
        $scope.viewType = "add";
        setFocus();
    };
    
    $scope.applyFilter = function() {
        var filtered = [];
        $scope.setSelectedItem(null);
        
        if ($scope.filter) {
            var filtro = $scope.filter.toLowerCase();
            itens.forEach(function(item) {
                if (item.name.toLowerCase().indexOf(filtro) > -1) filtered.push(item);
            });
        } else {
            filtered = itens;
        }
        
        $scope.itensFiltered = filtered;
    };    
    
    $scope.back = function() {
        $scope.viewType = "list";
    };
    
    var findIndex = function(id, collection) {
        if (collection === undefined) collection = itens;
        var index = collection.findIndex(function(item) {
            return item.id === id;
        });
        
        return index;
    };
    
    $scope.delete = function(id) {
        var yesFunction = function() {
            $scope.setSelectedItem(null);
            var index = findIndex(id);
            itens.splice(index, 1);    
            modal.close();
        };
        
        var modal = AppService.openModalMessage(AppConstants.MESSAGES.CONFIRM_DELETE, AppConstants.MODAL_MESSAGE.QUESTION, yesFunction);
    };
    
    $scope.edit = function(item) {
        $scope.formCrud.$submitted = false;
        $scope.setSelectedItem(item);
        $scope.item = {};
        angular.copy(item, $scope.item);
        if ($scope.item.salario) $scope.item.salario = $filter("number")($scope.item.salario, 2);
        $scope.viewType = "edit";
        setFocus();
    };    
    
    $scope.setSelectedItem = function(item) {
        if ($scope.selectedItem) {
            $scope.selectedItem.selected = false;
        }
        if (item === null) {
            $scope.selectedItem = null;
        } else {
            if ($scope.selectedItem && $scope.selectedItem.id === item.id) {
                $scope.selectedItem = null;
            } else {
                item.selected = true;
                $scope.selectedItem = item;
            }
        }        
    };
    
    $scope.submitForm = function() {
        if ($scope.formCrud.$invalid) return;
        
        var item = {};
        angular.copy($scope.item, item);
        if ($scope.viewType === "add") {
            item.id = uuid();
            itens.push(item);
        } else {
            var index = findIndex(item.id);
            itens[index] = item;
        }

        $scope.applyFilter();
        var index = findIndex(item.id, $scope.itensFiltered);
        if (index > -1) {
            $scope.setSelectedItem(item);
        } else {
            $scope.setSelectedItem(null);
        }
        
        $scope.back();
    };
    
    $http.get("assets/data.json?" + new Date())
    .then(function(response) {
        itens = response.data;
        $scope.itensFiltered = itens;
    }, 
    function(response) {
        AppService.openModalMessage(AppConstants.MESSAGES.RESPONSE_ERROR);
    });    
});

app.controller("LoginController", function($scope, AppConstants, AppService) {
    $scope.MESSAGES = {REQUIRED: AppConstants.MESSAGES.REQUIRED};
    
    $scope.doLogin = function(user) {
        if (user.login === 'f' && user.senha === '1') {
            user.name = 'Fabio da Silva';
            AppService.setLoggedUser(user);
            AppService.loadRoute("employees");
        } else {
            AppService.openModalMessage(AppConstants.MESSAGES.LOGIN_NOT_FOUND);
        }
    };
});

/* --------------------------------------------------------------------------------------------------- */

function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
}

/* 
 * input = jQuery object
 * Fonte: https://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
 */
function cursorToEnd(input) {
    input.focus();

    // If this function exists...
    if (input.setSelectionRange) {
        // ... then use it (Doesn't work in IE)
        // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
        var len = $(input).val().length * 2;

        input.setSelectionRange(len, len);
    } else {
        // ... otherwise replace the contents with itself
        // (Doesn't work in Google Chrome)
        $(input).val($(input).val());
    }

    // Scroll to the bottom, in case we're in a tall textarea
    // (Necessary for Firefox and Google Chrome)
    input.scrollTop = 999999;        
}

function focus(jQueryObject) {
    setTimeout(function() {
        cursorToEnd(jQueryObject);
    }, 500);
};

function leftPad(str, size, padChar) {
    if (!padChar) {
        padChar = " ";
    }

    var arr = new Array(size);
    var valorTemp = arr.join(padChar) + str;

    return valorTemp.substr(valorTemp.length - size);
};

function uuid() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";
    return s.join("");
}